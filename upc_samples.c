#include <stdio.h>
#include <stdlib.h>

#define END_OF_SAMPLES_SENTINEL -1
#define DIGITS_IN_UPC_CODE 12

void print_samples( int s[][DIGITS_IN_UPC_CODE] );

int main()
{
	int sample_upc_codes[][DIGITS_IN_UPC_CODE] = {
		0,7,2,9,7,9,0,0,0,1,8,3,
		0,3,8,0,0,0,5,5,3,7,5,2,
		0,7,2,7,9,9,3,3,5,2,3,6,
		3,0,5,2,1,6,0,8,5,0,0,2,
		7,0,1,0,7,7,7,0,1,0,7,7,
		0,7,9,4,0,0,8,0,4,5,0,1,
		4,1,2,3,4,5,5,4,3,2,1,0,
		0,2,4,0,0,0,1,6,2,8,6,0,
		0,1,1,1,1,0,8,5,6,8,0,7,
		0,8,1,2,2,7,0,5,6,7,2,8,
		0,5,1,0,0,0,1,3,8,1,0,1,
		0,7,2,4,3,8,3,4,4,4,5,2,
		7,4,7,3,1,3,5,0,1,0,2,1,
		END_OF_SAMPLES_SENTINEL,0,0,0,0,0,0,0,0,0,0,0,
	};
	
	print_samples( sample_upc_codes );

	return EXIT_SUCCESS;
}

void print_samples( int s[][DIGITS_IN_UPC_CODE] )
{
	int i, j;

	i = j = 0;
	while ( s[i][0] != END_OF_SAMPLES_SENTINEL ) {
		printf( "s[%2d] = ", i );
		do {
			printf( "%d", s[i][j++] );
		} while ( j < DIGITS_IN_UPC_CODE );
		puts( "" );
		i++;
		j = 0;
	}

	return;
}